﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SMBookingAPI.Domain.Models;
using SMBookingAPI.Resources;

namespace SMBookingAPI.Mapping
{
     public class ModelToResourceProfile : Profile
     {
         public ModelToResourceProfile()
         {
             CreateMap<Hotel, HotelResource>();
             CreateMap<Room, RoomResource>();
             CreateMap<Booking, BookingResource>();

         }
     }
}
