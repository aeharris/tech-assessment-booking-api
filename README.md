### Alice Harris Booking API

Booking API created for technical assessment.

This Readme contains instructions for building from source, seeding the database, finding the web app online, and querying the API. The query documentation aso includes some musings on assumptions I made during development.

##### Instructions For Building And Running From Source

The simplest way to run the project is to open the solution in Visual Studio. I used Visual Studio 2019, and needed to install ASP.NET Core 2.1 and EF Core 2.1. Specifically I used `Microsoft.Entity.FrameworkCore.Sqlite`. I also used `AutoMapper.Extensions.Microsoft.DependencyInjection`.  

It may prompt you to install these and any other required dependencies, I am unsure. I've never built a project like this in this IDE before, I'm unsure how it handles dependencies when first opening a solution. If not, they can be found in `Tools > Nuget Package Manager > Manage Nuget Packages For Solution...`, and search for them to install.

Once they're installed, or if everything is already set up, the project can be run with `Ctrl+F5`. This will open a `localhost` tab on your browser, set to `/api/Bookings`, which simply lists all bookings in the database. If this appears without an issue, the app is running as intended.

##### Seeding The Database From Source

When the app is first loaded, the entire database is created from scratch, and seeded with data found in the `OnModeCreating` method of `SMBookingAPIContext.cs`. To change the seeded data, it can be replaced there before startup. I chose this method as it is quick and simple for a testing application like this. Were I to create this for a production database, I would instead handle the database creation and seeding with migrations.  
Note: When you manually edit the seed data like this, the database will only throw an error if you violate its constraints, eg two rooms with the same ID. It will not result in an error if you create two bookings that overlap, as it has no way of knowing this is incorrect at the database level. The API itself will not allow such an overlap when posting new bookings. My pre-created seed data also does not have overlaps, and covers all the cases I thought of while testing.

#### Finding The Web App Online

The API is currently up and running as an Azure App Service at the following url:

https://smbookingapi.azurewebsites.net

You will get a 404 error on loading because there is no webpage there. The relevant api pages that will simply return lists of data are:

https://smbookingapi.azurewebsites.net/api/Hotels  
https://smbookingapi.azurewebsites.net/api/Bookings  
https://smbookingapi.azurewebsites.net/api/Rooms  

#### Querying The API, And Assumptions Made

For querying the API, I figured it was easiest to use Postman, and as such, I created some documentation at the link below to help with queries:

https://documenter.getpostman.com/view/8356894/SVYtLcri?version=latest

I also added some comments about my assumptions of the brief, and decisions I made along the way. Hopefully it's a helpful guide to the application!