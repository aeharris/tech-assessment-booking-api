﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SMBookingAPI.Domain.Models;
using SMBookingAPI.Models;
using SMBookingAPI.Resources;
using AutoMapper;

// This controller handles all request relating to rooms.
// It can retrieve all rooms, and rooms by ID.
// Along with the specifically required ability to search rooms available between dates for given number of people.
// Other CRUD operations are not implemented, as being beyond the application scope.

namespace SMBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomsController : ControllerBase
    {
        private readonly SMBookingAPIContext _context;
        private readonly IMapper _mapper;

        public RoomsController(SMBookingAPIContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // Retrieves all rooms
        // GET: api/Rooms
        [HttpGet]
        public IEnumerable<RoomResource> GetRooms()
        {
            var rooms = _context.Rooms.Include(r => r.Hotel);
            var resources = _mapper.Map<IEnumerable<Room>, IEnumerable<RoomResource>>(rooms);
            return resources;
        }

        // Rerieves room with given id
        // GET: api/Rooms/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRoom([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var room = await _context.Rooms.Include(r => r.Hotel).SingleAsync(r => r.Id == id);

            if (room == null)
            {
                return NotFound();
            }

            var resource = _mapper.Map<Room, RoomResource>(room);

            return Ok(resource);
        }

        // Returns all rooms available between given dates, with enough capacity for given number of guests.
        // GET: api/Rooms/search/
        [Route("search")]
        [HttpGet]
        public async Task<IActionResult> GetRoomsBySearch([FromQuery] DateTime checkIn, DateTime checkOut, int numOfGuests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Query returns only rooms that do not have bookings overlapping associated days, and have enough capacity
            var rooms = await _context.Rooms
                .Include(r => r.Hotel) // Include associated Hotel to return hotel name and ID to client
                .Where(r => r.Capacity >= numOfGuests) // Room must have enough capacity for all guests
                .Where(r => !r.Bookings.Any(b => (b.CheckIn < checkOut && b.CheckOut > checkIn))) // Room must not have booking overlapping given dates
                .ToListAsync();

            if (rooms == null)
            {
                return NotFound();
            }

            // Map to RoomResource so as not to return unnecessary data
            var resources = _mapper.Map<IEnumerable<Room>, IEnumerable<RoomResource>>(rooms);
            return Ok(resources);
        }
    }
}