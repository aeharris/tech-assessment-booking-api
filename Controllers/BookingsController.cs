﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SMBookingAPI.Domain.Models;
using SMBookingAPI.Models;
using SMBookingAPI.Resources;
using AutoMapper;

// This controller handles all request relating to bookings.
// Bookings can be searched for by ID (booking reference), and also all bookings may be retrieved.
// Bookings can be added, provided they fulfil criteria.
// Other CRUD operations are not implemented, as being beyond application scope.

namespace SMBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly SMBookingAPIContext _context;
        private readonly IMapper _mapper;

        public BookingsController(SMBookingAPIContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // Returns all bookings
        // GET: api/Bookings
        [HttpGet]
        public IEnumerable<BookingResource> GetBookings()
        {
            var bookings = _context.Bookings.Include(b => b.Room).ThenInclude(r => r.Hotel);
            var resources = _mapper.Map<IEnumerable<Booking>, IEnumerable<BookingResource>>(bookings);
            return resources;
        }

        // Returns a booking given a reference (id)
        // GET: api/Bookings/ref/5
        [Route("ref")]
        [HttpGet]
        public async Task<IActionResult> GetBooking([FromQuery] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var booking = await _context.Bookings.Include(b => b.Room).ThenInclude(r => r.Hotel).SingleAsync(b => b.Id == id);

            if (booking == null)
            {
                return NotFound();
            }

            var resource = _mapper.Map<Booking, BookingResource>(booking);
            return Ok(resource);
        }

        // POST: api/Bookings
        [HttpPost]
        public async Task<IActionResult> PostBooking([FromBody] SaveBookingResource resource)
        {

            // Input validation, checks in sequence that:
            // The input fits the model
            // The dates are not backwards
            // The room exists
            // There are a valid number of guests for that room
            // The room is actually available
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            } else if (resource.CheckIn >= resource.CheckOut)
            {
                return BadRequest(new { error = "Invalid dates - first night cannot be later than last night" });
            } else if (!_context.Rooms.Any(r => r.Id == resource.RoomID))
            {
                return BadRequest(new { error = "Invalid room id - does not exist" });
            } else if ((resource.NumOfGuests > _context.Rooms.Single(r => r.Id == resource.RoomID).Capacity) || (resource.NumOfGuests <= 0))
            {
                return BadRequest(new { error = "Invalid numofguests - cannot be 0 or less, or larger than room capacity" });
            }

            if (_context.Bookings.Any(b => (b.RoomID == resource.RoomID && b.CheckIn < resource.CheckOut && b.CheckOut > resource.CheckIn)))
            {
                return BadRequest(new { error = "Invalid dates - room already booked" });
            }

            var booking = _mapper.Map<SaveBookingResource, Booking>(resource);
            _context.Bookings.Add(booking);
            await _context.SaveChangesAsync();

            var retrievedBooking = await _context.Bookings.Include(b => b.Room).ThenInclude(r => r.Hotel).SingleAsync(b => b.Id == booking.Id);
            var bookingResource = _mapper.Map<Booking, BookingResource>(retrievedBooking);
            return Ok(bookingResource);
        }
    }
}