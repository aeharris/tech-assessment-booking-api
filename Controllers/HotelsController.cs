﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SMBookingAPI.Domain.Models;
using SMBookingAPI.Models;
using SMBookingAPI.Resources;
using AutoMapper;

// This controller handles all requests relating to hotels.
// It allows all hotels to be returned, or searched by id or name.
// Remaining CRUD operations not implemented as beyond application scope.
// Also contains three requests that will empty and reseed the database.

namespace SMBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelsController : ControllerBase
    {
        private readonly SMBookingAPIContext _context;
        private readonly IMapper _mapper;

        public HotelsController(SMBookingAPIContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Hotels
        [HttpGet]
        public IEnumerable<HotelResource> GetHotels()
        {
            var hotels = _context.Hotels;
            var resources = _mapper.Map<IEnumerable<Hotel>, IEnumerable<HotelResource>>(hotels);
            return resources;
        }

        // GET: api/Hotels/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetHotel([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hotel = await _context.Hotels.FindAsync(id);

            if (hotel == null)
            {
                return NotFound();
            }

            var resource = _mapper.Map<Hotel, HotelResource>(hotel);

            return Ok(resource);
        }

        // This get request allows for hotels to be searched by name.
        // If multiple hotels with the same name exist, all will be returned.
        // Returns a HotelResource not a raw Hotel, to avoid returning unnecessary associated Room data
        // GET: api/Hotels/search/
        [Route("search")]
        [HttpGet]
        public async Task<IActionResult> GetHotelsByName([FromQuery] string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var hotels = await _context.Hotels.Where(h => h.Name.Contains(name)).ToListAsync();

            if (hotels == null)
            {
                return NotFound();
            }

            // Maps to HotelResource so as not to return unnecessary data
            var resources = _mapper.Map<IEnumerable<Hotel>, IEnumerable<HotelResource>>(hotels);
            return Ok(resources);
        }

        // WILL DELETE ALL RECORDS IN THE DATABASE
        // DELETE: api/Hotels/DELETEALL
        [HttpDelete("DELETEALL")]
        public async Task<IActionResult> DeleteAll()
        {

            _context.Database.ExecuteSqlCommand("delete from Hotels");
            await _context.SaveChangesAsync();

            return Ok();
        }

        // WILL DELETE ALL BOOKINGS IN THE DATABASE
        // Method added because there's no way to add hotels and rooms back without reseeding or additional functionality
        // DELETE: api/Hotels/DELETEALLBOOKINGS
        [HttpDelete("DELETEALLBOOKINGS")]
        public async Task<IActionResult> DeleteAllBookings()
        {

            _context.Database.ExecuteSqlCommand("delete from Bookings");
            await _context.SaveChangesAsync();

            return Ok();
        }

        // WILL ENTIRELY DELETE AND RESEED THE DATABASE
        // Post: api/Hotels/SEEDALL
        [HttpPost("SEEDALL")]
        public async Task<IActionResult> SeedAll()
        {

            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}