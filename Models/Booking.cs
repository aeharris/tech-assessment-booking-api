﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMBookingAPI.Domain.Models
{
    public class Booking
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime CheckIn { get; set; }
        [DataType(DataType.Date)]
        public DateTime CheckOut { get; set; }
        public int NumOfGuests { get; set; }

        public int RoomID { get; set; }
        public Room Room { get; set; }
    }
}
