﻿using System.Collections.Generic;

namespace SMBookingAPI.Domain.Models
{
    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<Room> Rooms { get; set; } = new List<Room>();
    }
}
