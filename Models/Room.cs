﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMBookingAPI.Domain.Models
{
    public class Room
    {
        public int Id { get; set; }
        public String RoomType { get; set; }
        public int Capacity { get; set; }

        public int HotelID { get; set; }
        public Hotel Hotel { get; set; }

        public IList<Booking> Bookings { get; set; } = new List<Booking>();
    }
}
