﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SMBookingAPI.Domain.Models;

// Data returned by the API when Hotels are requested.
// Same as Hotel except stripped of unnecessary Room data

namespace SMBookingAPI.Resources
{
    public class HotelResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
