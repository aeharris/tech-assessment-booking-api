﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

// Data returned by API when Bookings are requested.
// Same as Booking but returns RoomResource instead of Room.
// This avoids duplicating the RoomID in json response.

namespace SMBookingAPI.Resources
{
    public class BookingResource
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime CheckIn { get; set; }
        [DataType(DataType.Date)]
        public DateTime CheckOut { get; set; }
        public int NumOfGuests { get; set; }
        public RoomResource Room { get; set; }
    }
}
