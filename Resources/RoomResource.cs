﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Data returned when Rooms are requested.
// Same as Room, but stripped of Booking data
// Associated Hotel returns HotelResource as well

namespace SMBookingAPI.Resources
{
    public class RoomResource
    {
        public int Id { get; set; }
        public string RoomType { get; set; }
        public int Capacity { get; set; }
        public HotelResource Hotel { get; set; }
    }
}
