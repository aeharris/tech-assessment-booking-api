﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

// Data requested by API when making Booking.
// SaveBookingResource is mapped to Booking.
// This adds an ID and the Room relation, then saves to db.

namespace SMBookingAPI.Resources
{
    public class SaveBookingResource
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime CheckIn { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime CheckOut { get; set; }
        [Required]
        public int NumOfGuests { get; set; }
        [Required]
        public int RoomID { get; set; }
    }
}
