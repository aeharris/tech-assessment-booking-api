﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SMBookingAPI.Domain.Models;

namespace SMBookingAPI.Models
{
    public class SMBookingAPIContext : DbContext
    {
        public SMBookingAPIContext (DbContextOptions<SMBookingAPIContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Set the filename of the database to be created
            optionsBuilder.UseSqlite("Data Source=hotelBookings.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Seed two hotels into the database
            modelBuilder.Entity<Hotel>().HasData(
                new Hotel { Id = 1, Name = "The Grand Budapest Hotel"},
                new Hotel { Id = 2, Name = "Hotel California" }
            );

            // Seed 12 rooms - 6 rooms per hotel
            modelBuilder.Entity<Room>().HasData(
                new Room { Id = 1, RoomType = "Single", Capacity = 1, HotelID = 1 },
                new Room { Id = 2, RoomType = "Single", Capacity = 1, HotelID = 1 },
                new Room { Id = 3, RoomType = "Single", Capacity = 1, HotelID = 1 },
                new Room { Id = 4, RoomType = "Double", Capacity = 2, HotelID = 1 },
                new Room { Id = 5, RoomType = "Double", Capacity = 2, HotelID = 1 },
                new Room { Id = 6, RoomType = "Deluxe", Capacity = 4, HotelID = 1 },
                new Room { Id = 7, RoomType = "Single", Capacity = 1, HotelID = 2 },
                new Room { Id = 8, RoomType = "Single", Capacity = 1, HotelID = 2 },
                new Room { Id = 9, RoomType = "Single", Capacity = 1, HotelID = 2 },
                new Room { Id = 10, RoomType = "Double", Capacity = 2, HotelID = 2 },
                new Room { Id = 11, RoomType = "Double", Capacity = 2, HotelID = 2 },
                new Room { Id = 12, RoomType = "Deluxe", Capacity = 4, HotelID = 2 }
            );

            // Seed 12 bookings - enough to test just about every search query combination of dates and number of guests
            modelBuilder.Entity<Booking>().HasData(
                new Booking { Id = 1, CheckIn = new DateTime(2019, 8, 9), CheckOut = new DateTime(2019, 8, 17), NumOfGuests = 1, RoomID = 1 },
                new Booking { Id = 2, CheckIn = new DateTime(2019, 8, 9), CheckOut = new DateTime(2019, 8, 24), NumOfGuests = 2, RoomID = 4 },
                new Booking { Id = 3, CheckIn = new DateTime(2019, 8, 19), CheckOut = new DateTime(2019, 8, 24), NumOfGuests = 4, RoomID = 12 },
                new Booking { Id = 4, CheckIn = new DateTime(2019, 8, 10), CheckOut = new DateTime(2019, 8, 13), NumOfGuests = 2, RoomID = 10 },
                new Booking { Id = 5, CheckIn = new DateTime(2019, 8, 9), CheckOut = new DateTime(2019, 8, 17), NumOfGuests = 2, RoomID = 5 },
                new Booking { Id = 6, CheckIn = new DateTime(2019, 8, 3), CheckOut = new DateTime(2019, 8, 17), NumOfGuests = 1, RoomID = 8 },
                new Booking { Id = 7, CheckIn = new DateTime(2019, 8, 21), CheckOut = new DateTime(2019, 8, 24), NumOfGuests = 1, RoomID = 3 },
                new Booking { Id = 8, CheckIn = new DateTime(2019, 8, 16), CheckOut = new DateTime(2019, 8, 24), NumOfGuests = 2, RoomID = 11 },
                new Booking { Id = 9, CheckIn = new DateTime(2019, 8, 6), CheckOut = new DateTime(2019, 8, 14), NumOfGuests = 3, RoomID = 6 },
                new Booking { Id = 10, CheckIn = new DateTime(2019, 8, 15), CheckOut = new DateTime(2019, 8, 19), NumOfGuests = 2, RoomID = 10 },
                new Booking { Id = 11, CheckIn = new DateTime(2019, 8, 1), CheckOut = new DateTime(2019, 8, 11), NumOfGuests = 1, RoomID = 7 },
                new Booking { Id = 12, CheckIn = new DateTime(2019, 8, 23), CheckOut = new DateTime(2019, 8, 31), NumOfGuests = 1, RoomID = 1 }
            );
        }

        public DbSet<SMBookingAPI.Domain.Models.Hotel> Hotels { get; set; }

        public DbSet<SMBookingAPI.Domain.Models.Room> Rooms { get; set; }

        public DbSet<SMBookingAPI.Domain.Models.Booking> Bookings { get; set; }
    }
}
